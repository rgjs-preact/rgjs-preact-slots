
import { Component } from 'preact';

/**
 * See https://preactjs.com/guide/v10/components
 * @class
 */
export class SlotContent extends Component {

  // ================================
  // CONSTRUCTOR

  /**
   * @constructor
   */
  constructor( props, context ) {
    super( props, context );
  }

  // ================================
  // LIFECYCLE

  /**
   * Lifecycle: Called whenever our component is created.
   * See https://preactjs.com/guide/v10/components#lifecycle-methods
   */
  componentDidMount() {
    this._registerChildren();
  }

  /**
   * Lifecycle: Called before render(). (deprecated)
   * See https://preactjs.com/guide/v10/components#lifecycle-methods
   */
  componentWillUpdate( nextProps ) {
    this._updateChildren( nextProps );
  }

  /**
   * Lifecycle: Called just before our component will be destroyed.
   * See https://preactjs.com/guide/v10/components#lifecycle-methods
   */
  componentWillUnmount() {
    this._updateChildren();
  }

  /**
   * Lifecycle: Render
   */
  render() {
    return null;
  }

  // ================================
  // PRIVATE

  /**
   * @private
   */
  _registerChildren() {

    const { slots } = this.context;
    const slotName = this._getSlotName();

    if ( slotName in slots ) {
      this._updateChildren();
    }
    else
      console.warn(`${ this.constructor.name }: no <Slot /> '${ slotName }'`);

  }

  /**
   * @private
   */
  _updateChildren( props = null ) {

    if ( !props ) props = this.props;

    const { slots } = this.context;
    const slotName = this._getSlotName( props );

    if ( slotName in slots ) {
      slots[ slotName ].children = props.children;
      slots[ slotName ].updateSlot();
    }

  }

  /**
   * @private
   */
  _getSlotName( props = null ) {
    if ( !props ) props = this.props;
    return props?.name || props?.slot || 'default';
  }

}
