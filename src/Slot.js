
import { Component } from 'preact';

/**
 * See https://preactjs.com/guide/v10/components
 * @class
 */
export class Slot extends Component {

  // ================================
  // CONSTRUCTOR

  /**
   * @constructor
   */
  constructor( props, context ) {

    super( props, context );

    // Prepare state
    this.state = {
      children: null,
    };

    // Bind methods
    const methods = [ '_updateSlot' ];
    methods.forEach( m => this[ m ] = this[ m ].bind( this ) );

  }

  // ================================
  // LIFECYCLE

  /**
   * Lifecycle: Called before the component gets mounted to the DOM. (deprecated)
   * See https://preactjs.com/guide/v10/components#lifecycle-methods
   */
  componentWillMount() {
    this._registerSlot();
  }

  /**
   * Lifecycle: Called before render(). (deprecated)
   * See https://preactjs.com/guide/v10/components#lifecycle-methods
   */
  componentWillUpdate( nextProps ) {
    this._updateSlot( nextProps );
  }

  /**
   * Lifecycle: Called prior to removal from the DOM.
   * See https://preactjs.com/guide/v10/components#lifecycle-methods
   */
  componentWillUnmount() {
    this._unregSlot();
  }

  /**
   * Lifecycle: Render
   */
  render( _, state ) {
    return state.children;
  }

  // ================================
  // PRIVATE

  /**
   * @private
   */
  _registerSlot() {

    const { slots } = this.context;
    const slotName = this._getSlotName();

    if ( slotName in slots )
      console.warn(`${ this.constructor.name }: duplicate <Slot /> '${ slotName }'`);
    else
      slots[ slotName ] = {
        updateSlot: this._updateSlot,
        children: null,
      };

  }

  /**
   * @private
   */
  _updateSlot( props = null ) {

    if ( !props ) props = this.props;

    const { slots } = this.context;
    const slotName = this._getSlotName(props);
    const slot = slots[ slotName ];

    this.setState({ children: slot.children });

  }

  /**
   * @private
   */
  _unregSlot() {

    const { slots } = this.context;
    const slotName = this._getSlotName();

    if ( slotName in slots )
      delete slots[ slotName ];

  }

  /**
   * @private
   */
  _getSlotName( props = null ) {
    if ( !props ) props = this.props;
    return props?.name || 'default';
  }

}
