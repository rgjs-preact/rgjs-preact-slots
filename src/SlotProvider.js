
import { Component, createContext } from 'preact';

export const SlotContext = createContext({
  slots: {},
});

/**
 * See https://preactjs.com/guide/v10/components
 * @class
 */
export class SlotProvider extends Component {

  // ================================
  // CONSTRUCTOR

  /**
   * @constructor
   */
  constructor(props) {

    super(props);

    // Prepare others
    this._context = {
      slots: {},
    };

  }

  // ================================
  // LIFECYCLE

  /**
   * Lifecycle: Render
   */
  render({ children }) {
    return (
      <SlotContext.Provider value={ this._context }>
        { children }
      </SlotContext.Provider>
    );
  }

  // ================================
  // PUBLIC

  /**
   * See https://preactjs.com/guide/v10/differences-to-react#no-contexttypes-needed
   */
  getChildContext() {
    return this._context;
  }

  // ================================
  // PRIVATE

}
