# rgjs-preact-slots

## Getting started

```bash
npm install rgjs-preact-slots
```

## Usage

### Reusable components

Toolbar.js:

```js
import { SlotProvider, Slot, SlotContent } from 'rgjs-preact-slots';

const Toolbar = ({ children }) => (
  <div class="toolbar">
    <SlotProvider>
      <div class="icon"><Slot name="icon" /></div>
      <div class="title"><Slot name="title" /></div>
      {children}
    </SlotProvider>
  </div>
);

Toolbar.Slot = SlotContent;

export Toolbar;
```

Page.js

```js
import { Toolbar } from './Toolbar.js';

const Page = () => (
  <>
    <Toolbar>
      <Toolbar.Slot slot="icon"><img src="path/to/image.jpg" /></Toolbar.Slot>
      <Toolbar.Slot slot="title">Title</Toolbar.Slot>
    </Toolbar>

    <div>Lorem ispum</div>
  </>
);
```

Output:

```html
<div class="toolbar">
  <div class="icon"><img src="path/to/image.jpg" /></div>
  <div class="title">Title</div>
</div>

<div>Lorem ispum</div>
```

### Good to know

#### Default slot

Create a `Slot` element without a name to create a default slot:

```html
<Slot name="with-name" />
<Slot />
```

Any `SlotContent` elements that do not target a specific slot will end up there:

```html
<SlontContent slot="with-name">Targets the 'with-name' slot</SlontContent>
<SlontContent>This will end up in the default slot</SlontContent>
```

## Contribute

[File a bug](https://gitlab.com/rgjs-preact/rgjs-preact-slots/-/issues) and I'll try to help.

## License

MIT &copy; REJH Gadellaa

Heavily inspired by the [preact-slots](https://github.com/developit/preact-slots) library.
